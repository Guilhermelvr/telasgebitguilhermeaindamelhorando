import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ValidarResgatePage } from './validar-resgate.page';

const routes: Routes = [
  {
    path: '',
    component: ValidarResgatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ValidarResgatePageRoutingModule {}
