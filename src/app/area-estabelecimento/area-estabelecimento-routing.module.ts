import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AreaEstabelecimentoPage } from './area-estabelecimento.page';

const routes: Routes = [
  {
    path: '',
    component: AreaEstabelecimentoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AreaEstabelecimentoPageRoutingModule {}
