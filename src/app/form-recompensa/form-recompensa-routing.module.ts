import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormRecompensaPage } from './form-recompensa.page';

const routes: Routes = [
  {
    path: '',
    component: FormRecompensaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormRecompensaPageRoutingModule {}
