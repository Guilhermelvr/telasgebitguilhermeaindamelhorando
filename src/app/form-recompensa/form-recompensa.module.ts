import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormRecompensaPageRoutingModule } from './form-recompensa-routing.module';

import { FormRecompensaPage } from './form-recompensa.page';
import { ReactiveFormsModule } from "@angular/forms";
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FormRecompensaPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [FormRecompensaPage]
})
export class FormRecompensaPageModule {}
