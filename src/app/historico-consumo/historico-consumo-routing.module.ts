import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistoricoConsumoPage } from './historico-consumo.page';

const routes: Routes = [
  {
    path: '',
    component: HistoricoConsumoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HistoricoConsumoPageRoutingModule {}
