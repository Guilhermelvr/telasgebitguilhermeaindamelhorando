import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AreaClientePage } from './area-cliente.page';

describe('AreaClientePage', () => {
  let component: AreaClientePage;
  let fixture: ComponentFixture<AreaClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaClientePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AreaClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
