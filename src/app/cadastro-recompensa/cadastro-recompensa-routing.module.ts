import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from "@angular/forms";
import { CadastroRecompensaPage } from './cadastro-recompensa.page';

const routes: Routes = [
  {
    path: '',
    component: CadastroRecompensaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), ReactiveFormsModule],
  exports: [RouterModule],
})
export class CadastroRecompensaPageRoutingModule {}
