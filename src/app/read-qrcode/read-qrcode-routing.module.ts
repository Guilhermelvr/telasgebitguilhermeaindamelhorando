import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReadQrcodePage } from './read-qrcode.page';
import {BarcodeScanner } from '@ionic-native/barcode-scanner/ngx'
import { from } from 'rxjs';
const routes: Routes = [
  {
    path: '',
    component: ReadQrcodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [BarcodeScanner]
})
export class ReadQrcodePageRoutingModule {}
