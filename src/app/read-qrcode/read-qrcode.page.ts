import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-read-qrcode',
  templateUrl: './read-qrcode.page.html',
  styleUrls: ['./read-qrcode.page.scss'],
})
export class ReadQrcodePage{

  qrData = null;
  createdCode = null;
  scannedCode = null;

  constructor(private barcodeScanner : BarcodeScanner){

  }



scanCode(){
    this.barcodeScanner.scan().then(barcodeData => {
    this.scannedCode = barcodeData.text;
})

}


  

}
