import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MarcarConsumoPage } from './marcar-consumo.page';

describe('MarcarConsumoPage', () => {
  let component: MarcarConsumoPage;
  let fixture: ComponentFixture<MarcarConsumoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarcarConsumoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MarcarConsumoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
