import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MarcarConsumoPage } from './marcar-consumo.page';

const routes: Routes = [
  {
    path: '',
    component: MarcarConsumoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MarcarConsumoPageRoutingModule {}
