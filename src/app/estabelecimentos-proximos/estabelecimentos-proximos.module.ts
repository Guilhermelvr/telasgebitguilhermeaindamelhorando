import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EstabelecimentosProximosPageRoutingModule } from './estabelecimentos-proximos-routing.module';

import { EstabelecimentosProximosPage } from './estabelecimentos-proximos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EstabelecimentosProximosPageRoutingModule
  ],
  declarations: [EstabelecimentosProximosPage]
})
export class EstabelecimentosProximosPageModule {}
