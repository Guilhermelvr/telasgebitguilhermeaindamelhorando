import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadastroTipoConsumoPage } from './cadastro-tipo-consumo.page';

const routes: Routes = [
  {
    path: '',
    component: CadastroTipoConsumoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadastroTipoConsumoPageRoutingModule {}
