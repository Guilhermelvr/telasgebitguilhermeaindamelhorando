import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CadastroTipoConsumoPage } from './cadastro-tipo-consumo.page';

describe('CadastroTipoConsumoPage', () => {
  let component: CadastroTipoConsumoPage;
  let fixture: ComponentFixture<CadastroTipoConsumoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroTipoConsumoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CadastroTipoConsumoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
