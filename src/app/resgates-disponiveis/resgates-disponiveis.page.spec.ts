import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ResgatesDisponiveisPage } from './resgates-disponiveis.page';

describe('ResgatesDisponiveisPage', () => {
  let component: ResgatesDisponiveisPage;
  let fixture: ComponentFixture<ResgatesDisponiveisPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResgatesDisponiveisPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ResgatesDisponiveisPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
