import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ReactiveFormsModule } from "@angular/forms";
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-form-tipo-consumo',
  templateUrl: './form-tipo-consumo.page.html',
  styleUrls: ['./form-tipo-consumo.page.scss'],
})
export class FormTipoConsumoPage {

  public loginForm: any;
  router: any;
  constructor(formBuilder: FormBuilder, public alertController: AlertController) {
    this.loginForm = formBuilder.group({
      nome: ['', Validators.required],
      pont: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(20),
      Validators.required])],
    });
   
  }
  async salva() {
    let { nome, pont } = this.loginForm.controls;

    if (!this.loginForm.valid) {
    }
    if (!nome.valid) {
      const alert = await this.alertController.create({
        header: 'Form Consumo',
       
        message: 'Preencha o campo nome ',
        buttons: ['OK']
      });
      await alert.present();
        
      } else if(!pont.valid) {
        const alert = await this.alertController.create({
          header: 'Form Consumo',
         
          message: 'Preencha o campo pontos ',
          buttons: ['OK']
        });
        await alert.present();
      }
    else {
      const alert = await this.alertController.create({
        header: 'Form Consumo',
    
        message: 'Salvo com sucesso ',
        buttons: ['OK']
      });
      await alert.present();
      
    }
  }
  }


