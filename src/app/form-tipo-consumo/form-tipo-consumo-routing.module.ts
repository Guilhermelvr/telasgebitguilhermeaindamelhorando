import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormTipoConsumoPage } from './form-tipo-consumo.page';

const routes: Routes = [
  {
    path: '',
    component: FormTipoConsumoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormTipoConsumoPageRoutingModule {}
