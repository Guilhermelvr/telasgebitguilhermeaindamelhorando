import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadastroEstabelecimentoPage } from './cadastro-estabelecimento.page';

const routes: Routes = [
  {
    path: '',
    component: CadastroEstabelecimentoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadastroEstabelecimentoPageRoutingModule {}
