import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadastroEstabelecimentoPageRoutingModule } from './cadastro-estabelecimento-routing.module';

import { CadastroEstabelecimentoPage } from './cadastro-estabelecimento.page';
import { ReactiveFormsModule } from "@angular/forms";
import { BrMaskerModule } from 'br-mask';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadastroEstabelecimentoPageRoutingModule,
    ReactiveFormsModule,
    BrMaskerModule
  ],
  declarations: [CadastroEstabelecimentoPage]
})
export class CadastroEstabelecimentoPageModule {}
